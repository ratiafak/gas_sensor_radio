S2LP Library
============

This folder contains S2LP driver from `STSW-S2LP-DK`, `Evaluation SW package based on S2-LP` version `1.3.2`.
Note that the `@version 1.3.0` appears in S2LP sources from 2017 as well as in these.

Three warnings reports were turned off for this folder:
```
-Wno-char-subscripts
-Wno-strict-aliasing
-Wno-format
```