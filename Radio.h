/**
 * @file Radio.h
 * @brief Wraps certain parts of the S2LP library.
 * @author Tomas Jakubik, inspired by examples provided with S2LP library
 * @date Sep 17, 2020
 */


#ifndef RADIO_H_
#define RADIO_H_

#include <cstdint>
#include <cstddef>
#include <S2LP_CORE_SPI.h>

/**
 * @brief Class to send and receive response.
 * Channel size and frequency deviation are fit for GMSK and FFT decoding with two samples per symbol.
 * @note S2LP library cannot be instantiated more than once.
 * @note This class cannot be instantiated more than once.
 * @note This library presumes that HAL_GetTick() runs in milliseconds.
 */
class Radio
{
public:
    enum class State
    {
        Shutdown,   ///< All lost, reinit required,    0.0025 uA
        Standby,    ///< FIFO lost, config retention,  0.5 uA
        Ready,      ///< Ready to do something,      350 uA
        Rx,         ///< Rx,                        7200 uA
        Tx,         ///< Tx (or Rx during CSMA),   20000 uA
        Cw,         ///< Tx calibration continuous wave
    };

    ///Result of Rx or Tx operation
    enum class Result
    {
        Working,    ///< Radio is still working
        Failure,    ///< Operation failed: Rx discarded, Tx CCA failed, timeout expired
        RxReceived, ///< Received data ready
        TxSent,     ///< Data transmitted
    };

    static const constexpr uint32_t MAX_LENGTH = 128;    ///< Limit of FIFO

private:
    class DisableInterrupt
    {
        static uint32_t count;  ///< Keep track of nested disable sections, initialized to 0 in .cpp
    public:
        DisableInterrupt& operator=(const DisableInterrupt &other) = delete;  ///< Cannot be copied
        DisableInterrupt(const DisableInterrupt &other) = delete;  ///< Cannot be copied
        DisableInterrupt();  ///< Declare nestable section with disabled radio interrupt
        ~DisableInterrupt(); ///< Reenable interrupt
    };


    uint32_t frequency_ch0; ///< Base frequency of channel 0
    uint32_t frequency_current; ///< Currently used frequency
    uint32_t datarate;    ///< Datarate of the communication

    State state;    ///< Current chip state

    bool tx_sent;       ///< True if packet was sent
    bool rx_received;   ///< True if Rx FIFO has valid content
    bool done;          ///< True if any radio operation has ended

    uint32_t timeout_state;    ///< Timeout used for SPI and state changes [ms]
    uint32_t operation_start;       ///< when operation started [ms]
    uint32_t operation_timeout;     ///< Timeout of current operation [ms]
    uint32_t operation_rx_timeout;  ///< Timeout for switching to Rx (used also in Tx CSMA) [ms]
    uint32_t operation_tx_timeout;  ///< Timeout for switching to Tx [ms]

public:
    /**
     * @brief Configure parameters, do not touch hardware yet.
     * @param hspi already initialized HAL SPI handle
     * @param timeout_state timeout for SPI and state changes [ms]
     */
    Radio(SPI_HandleTypeDef &hspi, uint32_t timeout_state = 3) :
        frequency_ch0(0),
        frequency_current(0),
        datarate(0),
        state(State::Shutdown),
        timeout_state(timeout_state)
    {
        S2LPSpiSetHalHandle(&hspi, timeout_state);
    }

    /**
     * @brief Nothing to destroy.
     */
    virtual ~Radio()
    {
    }

    /**
     * @brief Register callbacks and init S2LP.
     * @param frequency base frequency of channel 0 [Hz]
     * @param datarate bitrate of the communication [Baud/s]
     * @return true on success, false on error
     */
    bool init(uint32_t frequency, uint32_t datarate);

    /**
     * @brief Reinit radio.
     * Can be used only after setting frequency and datarate earlier.
     * @return true on success, false on error
     */
    bool init();

    /**
     * @brief Reinit radio and start transmitting CW continuous wave.
     * @param frequency cw frequency
     * @return true on success, false on error
     */
    bool cwStart(uint32_t frequency);

    /**
     * @brief Reinit radio.
     * Can be used after cwStart() to reinit to normal mode.
     * @return true on success, false on error
     */
    bool cwStop()
    {
        return init();
    }

    /**
     * @brief GPIO interrupt from the radio.
     * This should be called on EXTI interrupt.
     */
    void interruptHandler();

    /**
     * @brief Pool radio Rx or Tx operation and guard safety timeout.
     * @return Result::Working until an operation is done
     */
    Result poolResult();

    /**
     * @brief Switch to a receiving mode for a given time.
     * @param timeout receiving timeout [ms]
     * @param channel receive on this channel
     * @return true on success, false on error
     */
    bool receive(int32_t timeout, uint32_t channel);

    /**
     * @brief Get received packet.
     * @param data store data here
     * @param max_data limit maximal length
     * @return 0 if there are no data in FIFO or if max_data is not enough, length of data otherwise
     */
    size_t getReceivedData(uint8_t* data, size_t max_data = MAX_LENGTH);

    /**
     * @brief Send a packet with listen before talk.
     * @param buffer the packet
     * @param size number of bytes in buffer
     * @param channel send on this channel
     * @param wait multiplier 1 ~ 15 for length of the listen, Tlisten = wait*64/datarate
     * @return true on success, false on error
     */
    bool send(const uint8_t *buffer, size_t size, uint32_t channel, uint32_t wait);

    /**
     * @brief Put radio to standby mode.
     * No need to reinitialize after. FIFO is lost.
     * @return true on success, false on error
     */
    bool standby();

    /**
     * @brief Shutdown the radio.
     * The chip will be reinitialized on next use.
     */
    void shutdown();

    /**
     * @brief Get current radio state.
     * @return state
     */
    State getState() const
    {
        return state;
    }

private:

    /**
     * @brief  Fixed version of "Set the CCA length".
     * @param  xCcaLength the CCA length (a value between 1 and 15 that multiplies the CCA period).
     *     This parameter can be any value of @ref CsmaLength.
     * @retval None.
     */
    void FixedS2LPCsmaSetCcaLength(uint8_t xCcaLength);

    /**
     * @brief Force radio state to ready with timeout.
     * @return true on success, false on error
     */
    bool forceReady();

    /**
     * @brief Wakeup radio and put to ready state.
     * @return true on success, false on error
     */
    bool ready();
};


#endif /* RADIO_H_ */
