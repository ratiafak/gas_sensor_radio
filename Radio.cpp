/**
 * @file Radio.cpp
 * @brief Wraps certain parts of the S2LP library.
 * @author Tomas Jakubik, inspired by examples provided with S2LP library
 * @date Sep 17, 2020
 */

#include <Radio.h>
#include <S2LP_Config.h>
#include <cstring>

uint32_t Radio::DisableInterrupt::count = 0;    ///< Keep track of nested disable sections

/**
 * @brief Declare nestable section with disabled radio interrupt.
 */
Radio::DisableInterrupt::DisableInterrupt()
{
    LL_EXTI_DisableIT_0_31(RF_GPIO0_EXTI_Line);
    count++;
}

/**
 * @brief Reenable interrupt.
 */
Radio::DisableInterrupt::~DisableInterrupt()
{
    count--;
    if (count == 0)
    {
        LL_EXTI_EnableIT_0_31(RF_GPIO0_EXTI_Line);
        LL_EXTI_GenerateSWI_0_31(RF_GPIO0_EXTI_Line);   //Trigger manually in case interrupt was missed
    }
}

/**
 * @brief Reinit radio.
 * Can be used only after setting frequency and datarate earlier.
 * @return true on success, false on error
 */
bool Radio::init()
{
    if(frequency_ch0 == 0 || datarate == 0)
    {
        return false;   //Error, not initialized yet
    }
    else
    {
        return init(frequency_ch0, datarate);
    }
}

/**
 * @brief Register callbacks and init S2LP.
 * @param frequency base frequency of channel 0 [Hz]
 * @param datarate bitrate of the communication [Baud/s]
 * @return true on success, false on error
 */
bool Radio::init(uint32_t frequency, uint32_t datarate)
{
    DisableInterrupt disable_irq;  //Disable interrupt for this function

    this->frequency_ch0 = frequency;
    this->datarate = datarate;

    //Reset
    LL_GPIO_SetOutputPin(RF_SHDN_GPIO_Port, RF_SHDN_Pin);
    HAL_Delay(3);
    LL_GPIO_ResetOutputPin(RF_SHDN_GPIO_Port, RF_SHDN_Pin);
    HAL_Delay(3);

    S2LPRadioSetXtalFrequency(52000000);  //Must be done before any radio init

    //Check product ID and that SPI is working
    if(S2LPGeneralGetDevicePN() != 0x03)
    {
        return false;
    }

    //Init library and the chip
    SRadioInit radio_init =
    {
        .lFrequencyBase    = frequency_ch0,     ///< Specifies the base carrier frequency in Hz
        .xModulationSelect = MOD_2GFSK_BT05,    ///< Specifies the modulation
        .lDatarate         = datarate,          ///< Specifies the datarate expressed in bps
        .lFreqDev          = datarate/4,        ///< Specifies the frequency deviation expressed in Hz
        .lBandwidth        = datarate*2,        ///< Specifies the channel filter bandwidth expressed in Hz
    };
    S2LPRadioInit(&radio_init);
    frequency_current = frequency_ch0;

    //Enable radio interrupts
    SGpioInit gpio_init =
    {
        .xS2LPGpioPin = S2LP_GPIO_0,
        .xS2LPGpioMode = S2LP_GPIO_MODE_DIGITAL_OUTPUT_LP,
        .xS2LPGpioIO = S2LP_GPIO_DIG_OUT_IRQ,
    };
    S2LPGpioInit(&gpio_init);
    S2LPIrqs irqs_init = { S_RESET };
    irqs_init.IRQ_RX_DATA_READY    = S_SET;
    irqs_init.IRQ_RX_DATA_DISC     = S_SET;
    irqs_init.IRQ_RX_TIMEOUT       = S_SET;
    irqs_init.IRQ_TX_DATA_SENT     = S_SET;
    irqs_init.IRQ_MAX_BO_CCA_REACH = S_SET;
    irqs_init.IRQ_RX_FIFO_ERROR    = S_SET;
    irqs_init.IRQ_TX_FIFO_ERROR    = S_SET;
    S2LPGpioIrqInit(&irqs_init);
    S2LPGpioIrqClearStatus();
    ///@note GPIO interrupt is inited to falling edge in main and enabled in destructor of disable_irq

    //Set power
    S2LPRadioSetMaxPALevel(S_ENABLE);   //Set maximal power
    S2LPRadioSetPALeveldBm(7, 14);
    S2LPRadioSetPALevelMaxIndex(7);

    //Packet config
    PktBasicInit basic_init =
    {
        .xPreambleLength      = 32,  ///< Set the preamble length of packet. From 1 to 1024 chip sequence.
        .xSyncLength          = 32,  ///< Set the sync word length of packet in bits. From 1 to 64 bits.
        .lSyncWords           = 0x8E666F38,  ///< Set the sync words in MSB.
        .xFixVarLength        = S_ENABLE,  ///< Enable the variable length mode.
        .cExtendedPktLenField = S_DISABLE,  ///< Extend the length field from 1 byte to 2 bytes. Variable length mode only.
        .xCrcMode             = PKT_CRC_MODE_16BITS_1,  ///< Set the CRC type. (CRC length 16 bits - poly: 0x8005)
        .xAddressField        = S_DISABLE,  ///< Enable the destination address field.
        .xFec                 = S_DISABLE,  ///< Enable the FEC/Viterbi.
        .xDataWhitening       = S_DISABLE,  ///< Enable the data whitening.
    };
    S2LPPktBasicInit(&basic_init);

    S2LPTimerLdcrMode(S_DISABLE);

    //Enable CSMA
    SCsmaInit csma_init =
    {
        .xCsmaPersistentMode = S_DISABLE, ///< Enable the CSMA persistent mode.
        .xMultiplierTbit     = CSMA_PERIOD_64TBIT, ///< Set the Tbit multiplier to obtain the Tcca.
        .xCcaLength          = 1, ///< Tcca multiplier to obtain Tlisten.
        .cMaxNb              = 0, ///< Max number of backoff cycles. From 0 to 7. (Only one attempt)
        .nBuCounterSeed      = 1, ///< BU counter seed. (Not used)
        .cBuPrescaler        = 0, ///< BU prescaler. From 0 to 63. (Not used)
    };
    S2LPCsmaInit(&csma_init);
    SRssiInit rssi_init =
    {
      .cRssiFlt       = 14,
      .xRssiMode      = RSSI_STATIC_MODE,
      .cRssiThreshdBm = -80,
    };
    S2LPRadioRssiInit(&rssi_init);

    //Verify ready state
    return forceReady();
}

/**
 * @brief Reinit radio and start transmitting CW continuous wave.
 * @param frequency cw frequency
 * @return true on success, false on error
 */
bool Radio::cwStart(uint32_t frequency)
{
    DisableInterrupt disable_irq;  //Disable interrupt for this function

    //Reset
    LL_GPIO_SetOutputPin(RF_SHDN_GPIO_Port, RF_SHDN_Pin);
    HAL_Delay(3);
    LL_GPIO_ResetOutputPin(RF_SHDN_GPIO_Port, RF_SHDN_Pin);
    HAL_Delay(3);

    S2LPRadioSetXtalFrequency(52000000);  //Must be done before any radio init

    //Check product ID and that SPI is working
    if(S2LPGeneralGetDevicePN() != 0x03)
    {
        return false;
    }

    //Init library and the chip
    SRadioInit radio_init =
    {
        .lFrequencyBase    = frequency,         ///< Specifies the base carrier frequency in Hz
        .xModulationSelect = MOD_NO_MOD,        ///< No modulation is CW
        .lDatarate         = 50000,
        .lFreqDev          = 25000,
        .lBandwidth        = 100000,
    };
    S2LPRadioInit(&radio_init);

    //Set power
    S2LPRadioSetMaxPALevel(S_ENABLE);   //Set maximal power
    S2LPRadioSetPALeveldBm(7, 14);
    S2LPRadioSetPALevelMaxIndex(7);

    //Start TX
    S2LPCmdStrobeTx();
    state = State::Cw;

    return true;
}

/**
 * @brief GPIO interrupt from the radio.
 * This should be called on EXTI interrupt.
 */
void Radio::interruptHandler()
{
    if((state == State::Shutdown) || (state == State::Cw)   //Ignore GPIO interrupt if radio is powered off or in CW
        || (LL_GPIO_IsInputPinSet(RF_GPIO0_GPIO_Port, RF_GPIO0_Pin) != 0))  //GPIO IRQ is not active
    {
        return;
    }

    //Read and clear interrupt status
    S2LPIrqs irq_status;
    S2LPGpioIrqGetStatus(&irq_status);

    //Packet received, can theoretically happen in State::Tx during CCA
    if (irq_status.IRQ_RX_FIFO_ERROR)   //RX FIFO error
    {
        S2LPCmdStrobeFlushRxFifo();
    }
    else if (irq_status.IRQ_RX_DATA_READY)
    {
        uint32_t len = S2LPFifoReadNumberBytesRxFifo();
        if ((len > 0) && (len <= MAX_LENGTH))    //Only valid length
        {
            rx_received = true;
        }
    }

    //Interrupts RX_DATA_DISC and RX_TIMEOUT are failure and are not handled

    if (state == State::Tx)
    {
        //For cMaxNb=0 this interrupt is called on start of successful TX
        if ((irq_status.IRQ_MAX_BO_CCA_REACH) && (g_xStatus.MC_STATE != MC_STATE_READY))
        {
            //MC_STATE is usually 116 which is undocumented
            return;  //Ignore this interrupt, otherwise TX would be unintentionally aborted as soon as it started
        }
        //For MAX_BO_CCA_REACH in a MC_STATE=READY state, TX was not success

        if (irq_status.IRQ_TX_DATA_SENT)    //Successfully sent
        {
            tx_sent = true;
        }

        if (irq_status.IRQ_TX_FIFO_ERROR)   //TX FIFO error
        {
            S2LPCmdStrobeFlushTxFifo();
        }
    }

    done = true;
    ready(); //Put radio to ready state (should already be after successful RX or TX)
}

/**
 * @brief Pool radio Rx or Tx operation and guard safety timeout.
 * @return Result::Working until an operation is done
 */
Radio::Result Radio::poolResult()
{
    {
        DisableInterrupt disable_irq;  //Disable interrupt for using SPI
        S2LPRefreshStatus();    //Read g_xStatus
    }
    /**
     * @note Interrupts allowed to prevent race.
     * Interrupt handler might happen and radio will automatically switch to Ready.
     * For extra short packets it caused race and failed timeout.
     * Read status should be done before checking done flag.
     */

    if (done)
    {
        done = false;

        operation_timeout = 0;
        operation_rx_timeout = 0;
        operation_tx_timeout = 0;

        if (rx_received)    //Rx successfully finished
        {
            rx_received = false;
            tx_sent = false;
            return Result::RxReceived;
        }

        if (tx_sent)    //Tx successfully finished
        {
            tx_sent = false;
            return Result::TxSent;
        }

        return Result::Failure;
    }
    else
    {
        uint32_t operation_duration = HAL_GetTick() - operation_start;

        if (operation_timeout)  //Operation with timeout
        {
            if (operation_duration > operation_timeout)  //Check timeout
            {
                DisableInterrupt disable_irq;  //Disable interrupt for using SPI
                operation_timeout = 0;
                operation_rx_timeout = 0;
                operation_tx_timeout = 0;
                ready();
                return Result::Failure;
            }
        }

        if (operation_rx_timeout || operation_tx_timeout)   //State change timeout enabled
        {
            if (operation_tx_timeout)
            {
                if (g_xStatus.MC_STATE == MC_STATE_TX)  //State changed successfully
                {
                    operation_tx_timeout = 0;
                    operation_rx_timeout = 0;   //Tx comes after Rx, clear Rx timeout
                }
                else if (operation_duration > operation_tx_timeout)  //Check timeout
                {
                    DisableInterrupt disable_irq;  //Disable interrupt for using SPI
                    operation_timeout = 0;
                    operation_rx_timeout = 0;
                    operation_tx_timeout = 0;
                    ready();
                    return Result::Failure;
                }
            }

            if (operation_rx_timeout)
            {
                if (g_xStatus.MC_STATE == MC_STATE_RX)  //State changed successfully
                {
                    operation_rx_timeout = 0;
                }
                else if (operation_duration > operation_rx_timeout)  //Check timeout
                {
                    DisableInterrupt disable_irq;  //Disable interrupt for using SPI
                    operation_timeout = 0;
                    operation_rx_timeout = 0;
                    operation_tx_timeout = 0;
                    ready();
                    return Result::Failure;
                }
            }
        }
    }

    return Result::Working; //Still working
}

/**
 * @brief Switch to a receiving mode for a given time.
 * @param timeout receiving timeout [ms]
 * @param channel receive on this channel
 * @return true on success, false on error
 */
bool Radio::receive(int32_t timeout, uint32_t channel)
{
    DisableInterrupt disable_irq;  //Disable interrupt for this function

    rx_received = false;
    tx_sent = false;
    done = false;

    //Get from unknown state to ready
    if (ready() == false)
    {
        done = true;    //End with fail
        return false;
    }

    uint32_t frequency = frequency_ch0 + channel * 2 * datarate;
    if (frequency != frequency_current)
    {
        ///@note Cannot use S2LPRadioSetChannel() because it is imprecise.
        S2LPRadioSetFrequencyBase(frequency);  //Set frequency
        frequency_current = frequency;
    }

    S2LPCmdStrobeFlushRxFifo();   //Flush FIFO

    S2LPCsma(S_DISABLE);    //CSMA breaks receive for some reason

    operation_start = HAL_GetTick();
    operation_rx_timeout = timeout_state;   //Rx should start sooner than
    operation_tx_timeout = 0;   //No Tx
    if (timeout > 0)
    {
        operation_timeout = timeout + timeout_state;
        S2LPTimerSetRxTimerUs(timeout * 1000); //Set RX timeout
        S2LPRadioEnableSQI(S_ENABLE);
        S2LPTimerSetRxTimerStopCondition(RSSI_AND_SQI_ABOVE_THRESHOLD);
    }
    else
    {
        operation_timeout = 0;
        S2LPTimerSetRxTimerStopCondition(TIMEOUT_ALWAYS_STOPPED);
    }

    S2LPCmdStrobeRx();    //Receive
    state = State::Rx;

    return true;
}

/**
 * @brief Get received packet.
 * @param data store data here
 * @param max_data limit maximal length
 * @return 0 if there are no data in FIFO or if max_data is not enough, length of data otherwise
 */
size_t Radio::getReceivedData(uint8_t* data, size_t max_data)
{
    uint32_t len = S2LPFifoReadNumberBytesRxFifo();
    if((max_data < len) || (len == 0))
    {
        return 0;
    }

    S2LPSpiReadFifo(len, data);
    return len;
}

/**
 * @brief  Fixed version of "Set the CCA length".
 * @param  xCcaLength the CCA length (a value between 1 and 15 that multiplies the CCA period).
 *     This parameter can be any value of @ref CsmaLength.
 * @retval None.
 */
void Radio::FixedS2LPCsmaSetCcaLength(uint8_t xCcaLength)
{
  uint8_t tmp;
  s_assert_param(xCcaLength < 16);

  S2LPSpiReadRegisters(CSMA_CONF0_ADDR, 1, &tmp);

  tmp &= ~CCA_LEN_REGMASK;
  tmp |= (xCcaLength << 4);

  *(uint8_t*)&g_xStatus = S2LPSpiWriteRegisters(CSMA_CONF0_ADDR, 1, &tmp);

}

/**
 * @brief Send a packet with listen before talk.
 * @param buffer the packet
 * @param size number of bytes in buffer
 * @param channel send on this channel
 * @param wait multiplier 1 ~ 15 for length of the listen, Tlisten = wait*64/datarate, 0 for no CCA
 * @return true on success, false on error
 */
bool Radio::send(const uint8_t *buffer, size_t size, uint32_t channel, uint32_t wait)
{
    wait &= 0xf;
    if (size > MAX_LENGTH)
    {
        return false;
    }

    DisableInterrupt disable_irq;  //Disable interrupt for this function

    rx_received = false;
    tx_sent = false;
    done = false;

    //Get from unknown state to ready
    if (ready() == false)
    {
        done = true;    //End with fail
        return false;
    }

    uint32_t frequency = frequency_ch0 + channel * 2 * datarate;
    if (frequency != frequency_current)
    {
        ///@note Cannot use S2LPRadioSetChannel() because it is imprecise.
        S2LPRadioSetFrequencyBase(frequency);  //Set frequency
        frequency_current = frequency;
    }

    operation_start = HAL_GetTick();
    operation_rx_timeout = wait ? timeout_state : 0;    //Rx is used only if wait is used
    ///@note Tx timeout is time of CCA = wait * 64 / datarate.
    operation_tx_timeout = operation_rx_timeout + 1000 * 64 * wait / datarate + timeout_state;
    ///@note Operation timeout is time of the CCA + time of the packet = 8 * (preamble + syncword + payload + crc) / datarate.
    operation_timeout = operation_tx_timeout + 1000 * 8 * (size + 10) / datarate + timeout_state;
    if (wait > 0)
    {
        FixedS2LPCsmaSetCcaLength(wait);   //Set wait time in listen before talk
        S2LPCsma(S_ENABLE); //Collision sense enable
    }
    else
    {
        S2LPCsma(S_DISABLE);    //Disable collision sense
    }

    S2LPPktBasicSetPayloadLength(size); //Set TX packet size
    S2LPCmdStrobeFlushTxFifo();         //Flush FIFO
    //Data need to be copied because S2LPSpiWriteFifo() modifies buffer
    while(size)
    {
        uint32_t block_size = size > 16 ? 16 : size;
        uint8_t block[16];
        memcpy(block, buffer, block_size);
        S2LPSpiWriteFifo(block_size, block);     //Write packet to FIFO
        buffer += block_size;
        size -= block_size;
    }

    S2LPCmdStrobeTx();  //Start listen before talk and send the packet
    state = State::Tx;

    return true;
}

/**
 * @brief Put radio to standby mode.
 * No need to reinitialize after. FIFO is lost.
 * @return true on success, false on error
 */
bool Radio::standby()
{
    DisableInterrupt disable_irq;  //Disable interrupt for this function

    //Get from unknown state to ready
    if (ready() == false)
    {
        return false;
    }

    //Radio to standby, FIFO is lost
    S2LPCmdStrobeStandby();
    state = State::Standby;
    return true;
}

/**
 * @brief Shutdown the radio.
 * The chip will be reinitialized on next use.
 */
void Radio::shutdown()
{
    DisableInterrupt disable_irq;  //Disable interrupt for this function

    //No need to handle previous states, Shutdown is reset
    LL_GPIO_SetOutputPin(RF_SHDN_GPIO_Port, RF_SHDN_Pin);
    state = State::Shutdown;
}

/**
 * @brief Force radio state to ready with timeout.
 * @return true on success, false on error
 */
bool Radio::forceReady()
{
    //Force to ready state
    uint32_t start = HAL_GetTick();
    S2LPRefreshStatus();    //Read g_xStatus
    while(g_xStatus.MC_STATE != MC_STATE_READY)
    {
        if((HAL_GetTick() - start) > timeout_state)
        {
            return false;
        }

        S2LPCmdStrobeSabort();
        S2LPCmdStrobeReady();   //Strobing reads g_xStatus
    }

    state = State::Ready;
    return true;
}

/**
 * @brief Wakeup radio and put to ready state.
 * @return true on success, false on error
 */
bool Radio::ready()
{
    operation_timeout = 0;
    if((state == State::Shutdown) || (state == State::Cw))
    {
        return init();  //Reinit or wakeup radio
    }

    //Force radio into ready state
    if (forceReady() == false)
    {
        return init();  //Try to reinit stuck radio
    }
    else
    {
        S2LPIrqs irq_status;
        S2LPGpioIrqGetStatus(&irq_status);  //Clear interrupts by reading them
        return true;
    }
}

