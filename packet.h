/**
 * @file packet.h
 * @brief Description of packets used in gas_sensor.
 * @author Tomas Jakubik
 * @date Jan 8, 2021
 */

#ifndef PACKET_H_
#define PACKET_H_

#include <stdint.h>

#ifdef __cplusplus
 extern "C" {
#endif

/**
 * Packets for communication between gas_sensor and gas_sensor_1f_responder.
 * This first version handles raw data and is intended only for gathering for testing.
 */

#define FREQ_CHANNEL_FIRST    25    //Use this channel for testing the first version

/**
 * @brief Protocol header.
 */
typedef struct __attribute__((packed))
{
    uint8_t header;  ///< Packet header
    uint8_t id  :6;  ///< Sensor id
    uint8_t seq :2;  ///< Acknowledged sequence number
} proto_t;


/**
 * @brief Packet to request config.
 */
typedef struct __attribute__((packed))
{
    proto_t proto;  ///< Protocol header

    uint8_t channel;    ///< Channel where response is expected
    char fw_ver[16];    ///< Firmware version string
} request_t;

#define HEADER_REQUEST    0x37


#define GATHER_PAYLOAD_SIZE   31   ///< Maximal payload size

/**
 * @brief Packet to gather sensor data and transmit them to hub.
 */
typedef struct __attribute__((packed))
{
    proto_t proto;  ///< Protocol header

    int16_t temperature;            ///< Temperature [0.1 C]
    uint32_t vbat            :13;   ///< Battery voltage [mV]
    uint32_t secondly        : 5;   ///< Number of samples in second interval
    uint32_t channel         : 6;   ///< Channel where response is expected
    uint32_t humidity        : 7;   ///< Humidity [%]
    uint32_t                 : 1;

    /**
     * @brief Payload of timestamps [s] and LMP measured data [nA].
     * If secondly is zero, then payload is one uint16_t timestamp of first sample
     *    and at most GATHGER_PAYLOAD_SIZE-1 int16_t LMP samples in minute interval.
     *
     * If secondly holds number, then payload is one uint16_t timestamp of first sample
     *    and a number of int16_t LMP samples in second interval.
     *    Following is another uint16_t timestamp of first sample
     *    and a number of int16_t LMP samples in minute interval.
     */
    uint16_t payload[GATHER_PAYLOAD_SIZE];
} gather_t;

#define HEADER_GATHER    0x15


/**
 * @brief Packet to get an extra sample to hub.
 * Used when threshold is reached.
 */
typedef struct __attribute__((packed))
{
    proto_t proto;  ///< Protocol header

    uint16_t nearest_timestamp; ///< Approximate timestamp of this sample
    uint16_t extra_seq :10; ///< Extra iterator of extra samples
    uint16_t channel   : 6; ///< Channel where response is expected

    int16_t lmp;    ///< LMP data sample made now [nA]
} extra_t;

#define HEADER_EXTRA     0x13


/**
 * @brief Packet to control sensor settings and acknowledgment of gather_t.
 */
typedef struct __attribute__((packed))
{
    proto_t proto;  ///< Protocol header, response to packet with the same seq

    ///LMP config
    uint16_t bias                :4;
    uint16_t int_z               :2;
    uint16_t r_load              :2;
    uint16_t tia_gain            :3;
    uint16_t bias_positive       :1;
    uint16_t ref_source_external :1;

    uint16_t secondly            :1;    ///< Measure once a second instead of once a minute
    uint16_t multi_f             :1;    ///< True to transmit on random frequency
    uint16_t threshold_enable    :1;    ///< True to enable threshold

    int16_t threshold;  ///< Threshold to send extra values [nA]
} control_t;

#define HEADER_CONTROL   0x2e


/**
 * @brief Packet acknowledgment of gather_t without settings.
 */
typedef struct __attribute__((packed))
{
    proto_t proto;  ///< Protocol header, response to packet with the same seq
} ack_t;

#define HEADER_ACK       0x24


#ifdef __cplusplus
}
#endif

#endif /* PACKET_H_ */
